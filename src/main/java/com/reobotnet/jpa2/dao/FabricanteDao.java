package com.reobotnet.jpa2.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.reobotnet.jpa2.model.Fabricante;

public class FabricanteDao implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	@Inject
	public void salvar(Fabricante fabricante) {

		em.persist(fabricante);
	}

}
