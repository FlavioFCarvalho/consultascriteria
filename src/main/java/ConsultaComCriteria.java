import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.reobotnet.jpa2.model.Pessoa;
import com.reobotnet.jpa2.util.jpa.JPAUtil;

public class ConsultaComCriteria {
	
	
	public static void main(String[] args) {
		EntityManager em = JPAUtil.createEntityManager();
		
		// select, from, where, like... -> from(), where()..
		// JPQL: from Pessoa
		// JPQL: select c from Pessoa where c.nome like = 'Freitas%'
		
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Pessoa> criteriaQuery = builder.createQuery(Pessoa.class);
		Root<Pessoa> c = criteriaQuery.from(Pessoa.class);
		criteriaQuery.select(c);
		
	//	criteriaQuery.where(builder.like(c.<String>get("nome"), "Freitas%"));
		
		List<Pessoa> pessoas = em.createQuery(criteriaQuery).getResultList();
		
		for (Pessoa pessoa  : pessoas) {
			
			System.out.println();
			System.out.println("Código: " + pessoa.getCodigo());
			System.out.println("Nome: " + pessoa.getNome());
		}
		
		em.close();
	}
	

}
